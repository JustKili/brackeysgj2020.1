﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallSpawner : MonoBehaviour
{
    public GameObject[] topParts = new GameObject[3];
    public GameObject[] botParts = new GameObject[3];
    public GameObject wallContainer;

    public float distance = 30f;    //distance from player
    public float moveSpeed = 3f;    //movement speed of the walls

    public float spawnTime = 5f;    //in seconds
    private float curSpawnTime;

    // Start is called before the first frame update
    void Start()
    {
        curSpawnTime = spawnTime;
    }

    // Update is called once per frame
    void Update()
    {
        curSpawnTime -= Time.deltaTime;
        if(curSpawnTime <= 0f)
        {
            SpawnWall();
            curSpawnTime = spawnTime;
        }
    }

    void SpawnWall() {
        //two top parts, one flipped 180° on y axis
        //two bot parts, one flipped 180° on y axis
        GameObject tempContainer = Instantiate(wallContainer, Vector3.right * distance, Quaternion.identity);    //we'll move the container with all wallparts in it, just to make sure they don't move out of position
        tempContainer.GetComponent<WallContainer>().moveSpeed = moveSpeed;
        //cripple rotations for cripple blender models ^_^
        GameObject top1 = Instantiate(topParts[Random.Range(0, 3)], Vector3.right * distance, Quaternion.Euler(-90f, 0f, 0f), tempContainer.transform);
        GameObject top2 = Instantiate(topParts[Random.Range(0, 3)], Vector3.right * distance, Quaternion.Euler(-90f, 180f, 0f), tempContainer.transform);
        GameObject bot1 = Instantiate(botParts[Random.Range(0, 3)], Vector3.right * distance, Quaternion.Euler(-90f, 0f, 0f), tempContainer.transform);
        GameObject bot2 = Instantiate(botParts[Random.Range(0, 3)], Vector3.right * distance, Quaternion.Euler(-90f, 180f, 0f), tempContainer.transform);
    }
}
