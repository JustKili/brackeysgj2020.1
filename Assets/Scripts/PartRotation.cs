﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartRotation : MonoBehaviour
{
    public float rot_Speed = 100f;
    public KeyCode enableKey;
    public KeyCode input_Key_1;
    public KeyCode input_Key_2;
    bool stopUp = false;
    bool stopDown = false;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(input_Key_1) && !stopUp && Input.GetKey(enableKey))
        {
            transform.Rotate(Vector3.left * Time.deltaTime * rot_Speed);
          //  Debug.Log(transform.localEulerAngles.x);
            // transform.eulerAngles.Set(Mathf.Clamp(transform.eulerAngles.x, lower_Clamp, upper_Clamp),0,0);

            //  Mathf.Clamp(this.transform.rotation.eulerAngles.x, lower_Clamp, upper_Clamp);
        }
       

        if (Input.GetKey(input_Key_2) && !stopDown && Input.GetKey(enableKey))
        {
            transform.Rotate(Vector3.right * Time.deltaTime * rot_Speed);

        }
    }
    public void Stop()
    {
        Debug.Log("JOOOOOOOOOOOOOOOOONGE");
    }
    public void enterStopperUp()
    {
        stopUp = true;
    }

    public void leftStoperUp()
    {
        stopUp = false;
    }



    public void enterStopperDown()
    {
        stopDown = true;
    }

    public void leftStopperDown()
    {
        stopDown = false;
    }




}
