﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameVariables : MonoBehaviour
{
    public static int score = 0;
    public TextMeshProUGUI test;
    public static void UpdateScore(int value)
    {
        score += value;
    }
    private void Update()
    {
        test.text = score.ToString();
    }



}
