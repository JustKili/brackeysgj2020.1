﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotLockerDown : MonoBehaviour
{ 
    public string tag;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals(tag))
        {       
            Debug.Log("Enter");
            PartRotation p = other.GetComponent<PartRotation>();
            p.enterStopperDown();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals(tag))
        {
            Debug.Log("Exit");
            PartRotation p = other.GetComponent<PartRotation>();
            p.leftStopperDown();
        }
    }
}
